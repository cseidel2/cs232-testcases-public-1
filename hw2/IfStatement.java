/* This demonstrates how null pointer analysis is flow sensitive. 
Although f = new A(); is called before f.id(); is called, because of the "if" statement,
there is a null pointer error. */

class Main {
    public static void main(String[] a){
        A f;
        int res;

        if (false) {
            f = new A();
        } else {
            res = f.id();
        }
     
    }
}

class A {
    public int id(){
        return 1234;
    }
}
