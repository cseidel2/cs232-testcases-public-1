/* 	Nifty aspect of input-output pair:

	Running this program should result in a null-pointer exception
	due to the statement "b = c.nullCheck();" in method A.m1(). The
	variable c is a local variable of type C in this method. It is
	never instantiated in this method.
	
	Interestingly, the call to c.nullCheck() happens right AFTER a
	call to the method A.m2(). A local variable c of type C is also
	declared in method A.m2(). Unlike in the previous method, it is
	actually instantiated here.
	
	A correct null-pointer checker should not get confused between 
	these 2 "c" variables. Just because A.m2() instantiates a variable
	c, this does not make the local variable c in A.m1() not-null.
*/

class SameNameLocals {
	public static void main(String[] args) {
		boolean b;
		A a;
		
		a = new A();
		b = a.m2();
		b = a.m1();
		b = a.m2();
	}
}

class C {
	public boolean nullCheck() {
		return false;
	}
}

class A {
	public boolean m1() {
		boolean b;
		C c;
		
		b = this.m2();
		b = c.nullCheck();
		return b;
	}
	
	public boolean m2() {
		boolean b;
		C c;
		
		c = new C();
		b = c.nullCheck();
		return b;
	}
}
